package com.ck.mq.util;

import com.ck.mq.config.InitMqProducer;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.util.CollectionUtils;

/**
 * TODO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 16:14
 */
public class MqUtil {

    /**
     * 根据生成者唯一标识获取发送实例
     *
     * @param producerId
     * @return org.apache.rocketmq.client.producer.DefaultMQProducer
     * @throws
     * @author chenke
     * @date 2021/8/29 16:18
     */
    public static DefaultMQProducer getProducer(String producerId) {
        if (CollectionUtils.isEmpty(InitMqProducer.producerMap)) {
            return null;
        } else {
            return InitMqProducer.producerMap.get(producerId);
        }
    }

}
