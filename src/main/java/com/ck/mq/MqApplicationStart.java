package com.ck.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
public class MqApplicationStart {

	public static void main(String[] args) {
		SpringApplication.run(MqApplicationStart.class, args);
	}

}
