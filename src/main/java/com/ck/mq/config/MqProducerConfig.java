package com.ck.mq.config;

import com.ck.mq.vo.MqProducerVo;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 生产者配置
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/28 20:51
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "rocketmq.producer")
public class MqProducerConfig {

    private List<MqProducerVo> producerlist;

    public List<MqProducerVo> getProducerlist() {
        return producerlist;
    }

    public void setProducerlist(List<MqProducerVo> producerlist) {
        this.producerlist = producerlist;
    }
}
