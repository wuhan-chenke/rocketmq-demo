package com.ck.mq.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Arrays;
import java.util.List;

/**
 * Swagger接口设置
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/28 20:10
 */
@Configuration
public class Swagger3Config {

    @Bean
    public Docket createRestApi() {
        // 全局token参数

        return new Docket(DocumentationType.OAS_30)
                .globalRequestParameters(globalRequest())
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    private List<RequestParameter> globalRequest() {
        // 全局参数设置
        RequestParameter requestParameter = new RequestParameterBuilder()
                .name("token")
                .required(true)
                .description("token")
                .in(ParameterType.HEADER)
                .build();
        return Arrays.asList(requestParameter);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger3接口文档")
                .description("mq测试")
                .contact(new Contact("陈科", "http://127.0.0.1:8099/mq", "374303037@qq.com"))
                .version("1.0")
                .build();
    }
}
