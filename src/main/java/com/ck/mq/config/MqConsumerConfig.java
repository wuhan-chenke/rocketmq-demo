package com.ck.mq.config;

import com.ck.mq.vo.MqConsumerVo;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 生产者配置
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/28 20:51
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "rocketmq.consumer")
public class MqConsumerConfig {

    private List<MqConsumerVo> consumerlist;

    public List<MqConsumerVo> getConsumerlist() {
        return consumerlist;
    }

    public void setConsumerlist(List<MqConsumerVo> consumerlist) {
        this.consumerlist = consumerlist;
    }
}
