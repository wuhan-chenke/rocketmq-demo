package com.ck.mq.vo;

import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * TODO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 12:16
 */
public class MqConsumerParam {

    /**
     * 消息
     */
    private List<MessageExt> list;

    /**
     * 非顺序消费上下文
     */
    private ConsumeConcurrentlyContext consumeConcurrentlyContext;

    /**
     * 顺序消费上下文
     */
    private ConsumeOrderlyContext consumeOrderlyContext;

    public MqConsumerParam(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext, ConsumeOrderlyContext consumeOrderlyContext) {
        this.list = list;
        this.consumeConcurrentlyContext = consumeConcurrentlyContext;
        this.consumeOrderlyContext = consumeOrderlyContext;
    }

    public List<MessageExt> getList() {
        return list;
    }

    public void setList(List<MessageExt> list) {
        this.list = list;
    }

    public ConsumeConcurrentlyContext getConsumeConcurrentlyContext() {
        return consumeConcurrentlyContext;
    }

    public void setConsumeConcurrentlyContext(ConsumeConcurrentlyContext consumeConcurrentlyContext) {
        this.consumeConcurrentlyContext = consumeConcurrentlyContext;
    }

    public ConsumeOrderlyContext getConsumeOrderlyContext() {
        return consumeOrderlyContext;
    }

    public void setConsumeOrderlyContext(ConsumeOrderlyContext consumeOrderlyContext) {
        this.consumeOrderlyContext = consumeOrderlyContext;
    }

}
