package com.ck.mq.vo;

import java.util.List;

/**
 * 消费者配置VO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/28 23:16
 */
public class MqConsumerVo {

    /**
     * mq的groupName
     */
    private String groupName;
    /**
     * mq的nameserver地址
     */
    private String namesrvAddr;

    /**
     * 监听集合
     */
    private List<MqTopicVo> topics;

    /**
     * 消费者线程数据量
     */
    private Integer consumeThreadMin = 2;
    private Integer consumeThreadMax = 4;
    /**
     * 设置一次消费的条数，默认1
     */
    private Integer consumeMessageBatchMaxSize = 1;

    /**
     * 是否顺序消费
     */
    private Boolean orderly;

    /**
     * 消费模式
     */
    private String messageModel;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getNamesrvAddr() {
        return namesrvAddr;
    }

    public void setNamesrvAddr(String namesrvAddr) {
        this.namesrvAddr = namesrvAddr;
    }

    public List<MqTopicVo> getTopics() {
        return topics;
    }

    public void setTopics(List<MqTopicVo> topics) {
        this.topics = topics;
    }

    public Integer getConsumeThreadMin() {
        return consumeThreadMin;
    }

    public void setConsumeThreadMin(Integer consumeThreadMin) {
        this.consumeThreadMin = consumeThreadMin;
    }

    public Integer getConsumeThreadMax() {
        return consumeThreadMax;
    }

    public void setConsumeThreadMax(Integer consumeThreadMax) {
        this.consumeThreadMax = consumeThreadMax;
    }

    public Integer getConsumeMessageBatchMaxSize() {
        return consumeMessageBatchMaxSize;
    }

    public void setConsumeMessageBatchMaxSize(Integer consumeMessageBatchMaxSize) {
        this.consumeMessageBatchMaxSize = consumeMessageBatchMaxSize;
    }

    public Boolean getOrderly() {
        return orderly;
    }

    public void setOrderly(Boolean orderly) {
        this.orderly = orderly;
    }

    public String getMessageModel() {
        return messageModel;
    }

    public void setMessageModel(String messageModel) {
        this.messageModel = messageModel;
    }
}
