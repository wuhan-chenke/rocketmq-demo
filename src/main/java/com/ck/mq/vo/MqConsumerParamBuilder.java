package com.ck.mq.vo;

import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * TODO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 12:33
 */
public class MqConsumerParamBuilder {

    private List<MessageExt> list;
    private ConsumeConcurrentlyContext concurrentlyContext;
    private ConsumeOrderlyContext orderlyContext;

    public MqConsumerParamBuilder list(List<MessageExt> list) {
        this.list = list;
        return this;
    }

    public MqConsumerParamBuilder concurrentlyContext(ConsumeConcurrentlyContext concurrentlyContext) {
        this.concurrentlyContext = concurrentlyContext;
        return this;
    }

    public MqConsumerParamBuilder orderlyContext(ConsumeOrderlyContext orderlyContext) {
        this.orderlyContext = orderlyContext;
        return this;
    }

    public MqConsumerParam build() {
        return new MqConsumerParam(this.list, this.concurrentlyContext, this.orderlyContext);
    }

}
