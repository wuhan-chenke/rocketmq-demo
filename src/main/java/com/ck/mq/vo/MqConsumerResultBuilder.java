package com.ck.mq.vo;

/**
 * TODO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 13:22
 */
public class MqConsumerResultBuilder {

    private boolean success = false;

    private boolean reconsumeLater = true;

    private boolean saveConsumeLog = false;

    private String errMsg;

    private Throwable throwable;

    public MqConsumerResultBuilder success(boolean success) {
        this.success = success;
        return this;
    }

    public MqConsumerResultBuilder reconsumeLater(boolean reconsumeLater) {
        this.reconsumeLater = reconsumeLater;
        return this;
    }

    public MqConsumerResultBuilder saveConsumeLog(boolean saveConsumeLog) {
        this.saveConsumeLog = saveConsumeLog;
        return this;
    }

    public MqConsumerResultBuilder errMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

    public MqConsumerResultBuilder throwable(Throwable throwable) {
        this.throwable = throwable;
        return this;
    }

    public MqConsumerResult build() {
        return new MqConsumerResult(this.success, this.reconsumeLater, this.saveConsumeLog, this.errMsg, this.throwable);
    }

    public static MqConsumerResult success() {
        return new MqConsumerResultBuilder().success(true).build();
    }

}
