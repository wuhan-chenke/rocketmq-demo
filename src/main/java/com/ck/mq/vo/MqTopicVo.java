package com.ck.mq.vo;

/**
 * 主题
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/28 23:18
 */
public class MqTopicVo {

    private String topicName;

    private String tagName;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

}
