package com.ck.mq.vo;

/**
 * TODO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 15:51
 */
public class MqProducerVo {

    /**
     * 生产者唯一编号
     */
    private String producerId;

    private String groupName;

    // MQ注册中心
    private String namesrvAddr;

    // 消息最大值
    private Integer maxMessageSize;

    // 消息发送超时时间
    private Integer sendMsgTimeOut;

    // 失败重试次数
    private Integer retryTimesWhenSendFailed;

    public String getProducerId() {
        return producerId;
    }

    public void setProducerId(String producerId) {
        this.producerId = producerId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getNamesrvAddr() {
        return namesrvAddr;
    }

    public void setNamesrvAddr(String namesrvAddr) {
        this.namesrvAddr = namesrvAddr;
    }

    public Integer getMaxMessageSize() {
        return maxMessageSize;
    }

    public void setMaxMessageSize(Integer maxMessageSize) {
        this.maxMessageSize = maxMessageSize;
    }

    public Integer getSendMsgTimeOut() {
        return sendMsgTimeOut;
    }

    public void setSendMsgTimeOut(Integer sendMsgTimeOut) {
        this.sendMsgTimeOut = sendMsgTimeOut;
    }

    public Integer getRetryTimesWhenSendFailed() {
        return retryTimesWhenSendFailed;
    }

    public void setRetryTimesWhenSendFailed(Integer retryTimesWhenSendFailed) {
        this.retryTimesWhenSendFailed = retryTimesWhenSendFailed;
    }
}
