package com.ck.mq.vo;

import java.io.Serializable;

/**
 * TODO
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 9:59
 */
public class MqConsumerResult implements Serializable {

    /**
     * 是否处理成功
     */
    private boolean success = false;
    /**
     * 如果处理失败，是否允许消息队列继续调用，直到处理成功，默认true
     */
    private boolean reconsumeLater = true;
    /**
     * 是否需要记录消费日志，默认不记录
     */
    private boolean saveConsumeLog = false;

    /**
     * 错误消息
     */
    private String errMsg;
    /**
     * 错误堆栈
     */
    private Throwable e;

    public MqConsumerResult(boolean success, boolean reconsumeLater, boolean saveConsumeLog, String errMsg, Throwable e) {
        this.success = success;
        this.reconsumeLater = reconsumeLater;
        this.saveConsumeLog = saveConsumeLog;
        this.errMsg = errMsg;
        this.e = e;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isReconsumeLater() {
        return reconsumeLater;
    }

    public void setReconsumeLater(boolean reconsumeLater) {
        this.reconsumeLater = reconsumeLater;
    }

    public boolean isSaveConsumeLog() {
        return saveConsumeLog;
    }

    public void setSaveConsumeLog(boolean saveConsumeLog) {
        this.saveConsumeLog = saveConsumeLog;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Throwable getE() {
        return e;
    }

    public void setE(Throwable e) {
        this.e = e;
    }
}
