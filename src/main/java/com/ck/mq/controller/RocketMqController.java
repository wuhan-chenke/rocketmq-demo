package com.ck.mq.controller;

import com.ck.mq.util.MqUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 测试
 *
 * @author chenke
 * @version 1.0
 * @date 2021-03-15 15:13
 */
@Api(tags = "MQ测试类")
@RestController
@RequestMapping("/rocketmq")
public class RocketMqController {

    private static final Logger LOG = LoggerFactory.getLogger(RocketMqController.class);

    @ApiOperation("发送消息测试到主题ChenkeTopic")
    @GetMapping(value = "/sendChenkeTopic")
    public void sendChenkeTopic() throws Exception {
        String msg = "发生测试消息";
        Message sendMsg = new Message("ChenkeTopic", "test", msg.getBytes(RemotingHelper.DEFAULT_CHARSET));
        SendResult sendResult = MqUtil.getProducer("chenkeTest").send(sendMsg);
        LOG.info("发送消息结果:" + sendResult);
    }

    @ApiOperation("发送消息测试到主题sendChenkeTopic2")
    @GetMapping(value = "/sendChenkeTopic2")
    public void sendChenkeTopic2() throws Exception {
        String msg = "发生测试消息";
        Message sendMsg = new Message("ChenkeTopic2", "test", msg.getBytes(RemotingHelper.DEFAULT_CHARSET));

        SendResult sendResult = MqUtil.getProducer("chenkeTest").send(sendMsg);
        LOG.info("发送消息结果:" + sendResult);
    }

    @ApiOperation("批量发送消息测试")
    @GetMapping(value = "/sendChenkeTopic2Batch")
    public void sendChenkeTopic2Batch() throws Exception {
        List<Message> msgList = new ArrayList<>();
        for (int i = 1; i <= 3000; i++) {
            String msg = "发生测试消息" + i;
            Message sendMsg = new Message("ChenkeTopic2", "test", msg.getBytes(RemotingHelper.DEFAULT_CHARSET));
            msgList.add(sendMsg);

            if (i % 5 == 0) {
                SendResult sendResult = MqUtil.getProducer("chenkeTest").send(msgList);
                LOG.info("发送消息结果:" + sendResult);
                msgList.clear();
            }
        }
    }

    @ApiOperation("批量发送有序消息测试")
    @GetMapping(value = "/sendOrderlyBatch")
    public void sendOrderlyBatch() throws Exception {
        // 开16个线程，分别发生20组订单数据

        for (int i = 0; i < 16; i++) {
            int index = i;
            Thread thread = new Thread(() -> {

                // 模拟20个订单,每个订单隔5S发生一条消息，消息顺序：创建，付款，物流，完成
                for (int j = 0; j < 10; j++) {
                    try {
                        String createMsq = Thread.currentThread().getName() + ",订单编号" + index + "-" + j + ",创建";
                        String createKey = "orderNo" + index + j + "create";
                        Message createSendMsg = new Message("ChenkeTopic3", "order", createKey, createMsq.getBytes(RemotingHelper.DEFAULT_CHARSET));
                        // LOG.info(createMsq);
                        send(createSendMsg, j);


                        String paymentMsq = Thread.currentThread().getName() + ",订单编号" + index + "-" + j + ",付款";
                        String paymentKey = "orderNo" + index + j + "payment";
                        Message paymentSendMsg = new Message("ChenkeTopic3", "order", paymentKey, paymentMsq.getBytes(RemotingHelper.DEFAULT_CHARSET));
                        // LOG.info(paymentMsq);
                        send(paymentSendMsg, j);


                        String logisticsMsq = Thread.currentThread().getName() + ",订单编号" + index + "-" + j + ",物流";
                        String logisticsKey = "orderNo" + index + j + "logistics";
                        Message logisticsSendMsg = new Message("ChenkeTopic3", "order", logisticsKey, logisticsMsq.getBytes(RemotingHelper.DEFAULT_CHARSET));
                        // LOG.info(logisticsMsq);
                        send(logisticsSendMsg, j);


                        String completeMsq = Thread.currentThread().getName() + ",订单编号" + index + "-" + j + ",完成";
                        String completeKey = "orderNo" + index + j + "logistics";
                        Message completeSendMsg = new Message("ChenkeTopic3", "order", completeKey, completeMsq.getBytes(RemotingHelper.DEFAULT_CHARSET));
                        // LOG.info(completeMsq);
                        send(completeSendMsg, j);

                    } catch (Exception e) {
                        LOG.error("发生顺序消息失败，订单号:" + j, e);
                    }

                }

            });
            thread.setName("线程编号" + i);
            thread.start();
        }

    }

    private void send(Message msg, Integer orderNo) throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        SendResult sendResult = MqUtil.getProducer("chenkeTest").send(msg, new MessageQueueSelector() {
            @Override
            public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                // 根据订单id选择发送queue
                Integer id = (Integer) arg;
                int index = id % mqs.size();
                return mqs.get(index);
            }
        }, orderNo);
    }

    @ApiOperation("发送延迟消息")
    @GetMapping(value = "/sendDelayMeg")
    public void sendDelayMeg() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String msg = "发生延迟消息,延迟30分钟,当前时间" + sdf.format(new Date());
        Message sendMsg = new Message("ChenkeTopic2", "test", msg.getBytes(RemotingHelper.DEFAULT_CHARSET));
        // 延迟级别对应延迟时间 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
        sendMsg.setDelayTimeLevel(16);
        SendResult sendResult = MqUtil.getProducer("chenkeTest").send(sendMsg);
        LOG.info("发送消息结果:" + sendResult);
    }

}

