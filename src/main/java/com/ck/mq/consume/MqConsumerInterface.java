package com.ck.mq.consume;

import com.ck.mq.vo.MqConsumerParam;
import com.ck.mq.vo.MqConsumerResult;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public interface MqConsumerInterface {

    /**
     * 实际消费者继承该类
     *
     * @param param
     * @return com.ck.mq.vo.MqConsumerResult
     * @throws
     * @author chenke
     * @date 2021/8/29 10:25
     */
    MqConsumerResult handle(MqConsumerParam param);

}
