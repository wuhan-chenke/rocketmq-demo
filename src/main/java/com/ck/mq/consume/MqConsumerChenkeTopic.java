package com.ck.mq.consume;

import com.ck.mq.vo.MqConsumerParam;
import com.ck.mq.vo.MqConsumerResult;
import com.ck.mq.vo.MqConsumerResultBuilder;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 主题ChenkeTopic消费者
 *
 * @author chenke
 * @version 1.0
 * @date 2021/8/29 11:50
 */
@Component("mqConsumerHandle.ChenkeTopic")
public class MqConsumerChenkeTopic implements MqConsumerInterface {

    private static final Logger LOG = LoggerFactory.getLogger(MqConsumerChenkeTopic.class);

    @Override
    public MqConsumerResult handle(MqConsumerParam param) {
        // 消费逻辑自行完善，简单写下
        List<MessageExt> list = param.getList();
        for (MessageExt messageExt : list) {
            String topic = messageExt.getTopic();
            String tag = messageExt.getTags();
            try {
                String msg = new String(messageExt.getBody(), RemotingHelper.DEFAULT_CHARSET);
                LOG.info("消费者接收消息topic:{},tag:{},内容:{}", topic, tag, msg);
            } catch (Exception e) {
                LOG.error("消费消费失败,消息ID:" + messageExt.getMsgId(), e);
            }
        }
        return MqConsumerResultBuilder.success();
    }

}
